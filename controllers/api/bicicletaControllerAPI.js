var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = async function(req, res){
    const bicicletas = await Bicicleta.allBicis();
    res.status(200).json({
        bicicletas
    });
}

exports.bicicleta_create = async function (req, res){
    let bici = {
        code,
        color,
        modelo
    } = req.body;
    bici.ubicacion = [req.body.lat, req.body.lng];
    await Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = async function (req, res){
    await Bicicleta.removeByCode(req.body.id);
    res.status(204).send();
}

exports.bicicleta_update = async function (req, res){
    const bici = await Bicicleta.updateBici(req.params.id, req.body.color, req.body.modelo, req.body.ubicacion);
    res.status(200).json({
        bicicleta: bici
    })
}