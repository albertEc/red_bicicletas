const Usuario = require('../../models/usuario');

exports.usuarios_list = async function (req, res){
    const usuarios = await Usuario.find({});
    res.status(200).json({ usuarios });
};

exports.usuarios_create = async function (req, res){
    const usuario = new Usuario({ nombre: req.body.nombre, email: req.body.email, password: req.body.password });
    const usuarioNuevo = await usuario.save();
    res.status(200).json(usuarioNuevo);
};

exports.usuario_reservar = async function (req, res){
    const usuario = await Usuario.findById(req.body.id);
    const reserva = await usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta);
    res.status(200).json();
};