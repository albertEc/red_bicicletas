var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = async function (req, res){
    const bicis = await Bicicleta.allBicis();
    console.log(bicis);
    res.render('bicicletas/index', {bicis });
}

exports.bicicleta_create_get = function (req, res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res){
    console.log(req.body);
    var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = async function (req, res) {
    var bici = await Bicicleta.findById(req.params.id);
    console.log(bici);
    res.render('bicicletas/update', {bici});
}

exports.bicicleta_update_post = function (req, res){
    var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    res.redirect('/bicicletas');
}


exports.bicicleta_delete_post = function (req, res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}

