// Use at least Nodemailer v4.1.0
const nodemailer = require('nodemailer');

// Generate SMTP service account from ethereal.email
nodemailer.createTestAccount((err, account) => {
    if (err) {
        console.error('Failed to create a testing account. ' + err.message);
        return process.exit(1);
    }

    console.log('Credentials obtained, sending message...');

    // Create a SMTP transporter object
    let transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'thurman.klocko77@ethereal.email',
            pass: '4bZHfUYkBhFjZwDHrR'
        }
    });

    // Message object
    let message = {
        from: 'elbert.medhurst@ethereal.email',
        to: 'thurman.klocko77@ethereal.email',
        subject: 'Nodemailr is unicode friendly',
        text: 'Hola mundo...',
        html: '<p><b>Hola</b> mundo...</p>'
    };

    transporter.sendMail(message, (err, info) => {
        if (err) {
            console.log('Error occurred. ' + err.message);
            return process.exit(1);
        }

        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    });
});
