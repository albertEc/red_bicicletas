var map = L.map('main_map').setView([-3.680335, -79.682577], 16);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYWxiZXJ0OTkiLCJhIjoiY2toODVzeTRxMGEzdjJya2hnYmR4YXo4MSJ9.E1lSHhGQBgyKvGNWH07PTw'
}).addTo(map);

/*var marker = L.marker([-3.680331, -79.682570]).addTo(map);
var marker = L.marker([-3.680110, -79.682900]).addTo(map);
var marker = L.marker([-3.681200, -79.682580]).addTo(map); */

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})