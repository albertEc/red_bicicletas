const Bicicleta = require('../../models/bicicleta');
const Reserva = require('../../models/reserva');
const request = require('request');
const server = require('../../bin/www');
const Usuario = require('../../models/usuario');
const rp = require('request-promise');
var mongoose = require("mongoose");

describe('Usuarios API', () => {

    afterEach(async () => {
        await Bicicleta.deleteMany({});
        await Reserva.deleteMany({});
        await Usuario.deleteMany({});
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get('http://localhost:3000/api/usuarios', async function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.usuarios.length).toBe(0);
                done();
            });
        });
    });

    describe(' POST USUARIOS /create', () => {
        it('Status 200', (done) => {

            var headers = { 'content-type': 'application/json' };
            var nuevoUsuario = '{ "nombre": "Sebastian"}';

            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/usuarios/create',
                body: nuevoUsuario
            },
                function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var usuario = JSON.parse(body);
                    console.log(usuario);
                    expect(usuario.nombre).toBe('Sebastian');
                    done();
                });
        });
    });

    describe('Usuario reserva una bicicleta', () => {
        it('debe existir una reserva', async () => {

            const headers = { 'content-type': 'application/json' };

            const newUsuario = new Usuario({
                nombre: "Albert"
            });
            await newUsuario.save();
            const newBicicleta = new Bicicleta({
                code: 1,
                color: "azul",
                modelo: "urbana"
            });
            await newBicicleta.save();

            const hoy = new Date();
            const mañana = new Date();

            mañana.setDate(hoy.getDate() + 1);

            const postRequest = async(reserva) => {
                const urlPost = 'http://localhost:3000/api/usuarios/reservar';
                const headers = {'Content-Type': 'application/json'};
                const options = {
                    method: 'POST',
                    headers: headers,
                    json: true,
                    uri: urlPost,
                    body: reserva,
                    resolveWithFullResponse: true
                };

                const response = await rp(options);
                return response;
            }
            const reserva = {id: newUsuario.id, bici_id: newBicicleta.id, hoy, mañana};

            const result = await postRequest(reserva);
            expect(result.statusCode).toBe(200);
            const reservas = await Reserva.find({}).populate('bicicleta').populate('usuario');
            expect(reservas.length).toBe(1);
            expect(reservas[0].bicicleta.code).toBe(1);
            expect(reservas[0].usuario.nombre).toBe(newUsuario.nombre);

        });
    });

});