const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');
const Usuario = require('../../models/usuario');
const Reserva = require('../../models/reserva');

describe('Testing Usuarios', () => {

    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(async () => {
        await Bicicleta.deleteMany({});
        await Reserva.deleteMany({});
        await Usuario.deleteMany({});
    });

    describe('Usuario reserva una bicicleta', () => {
        it('debe existir una reserva', async () => {
            const newUsuario = new Usuario({
                nombre: "Albert"
            });
            await newUsuario.save();
            const newBicicleta = new Bicicleta({
                code: 1,
                color: "azul",
                modelo: "urbana"
            });
            await newBicicleta.save();

            const hoy = new Date();
            const mañana = new Date();

            mañana.setDate(hoy.getDate() + 1);
            await newUsuario.reservar(newBicicleta.id, hoy, mañana);
            const reservas = await Reserva.find({}).populate('bicicleta').populate('usuario');
            expect(reservas.length).toBe(1);
            expect(reservas[0].diasDeReserva()).toBe(2);
            expect(reservas[0].bicicleta.code).toBe(1);
            expect(reservas[0].usuario.nombre).toBe(newUsuario.nombre);
        });
    });


});